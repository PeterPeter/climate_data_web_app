# -*- coding: utf-8 -*-

import os,sys,importlib
import xarray as xr
import numpy as np

sys.path.append('../climate_data_on_shapes')
import class_on_shape; importlib.reload(class_on_shape)

COU = class_on_shape.shaped_object(iso='BEN', working_directory='small_data/BEN')
COU.read_shapefile('some_location_where_large_raw_data_is_stored/BEN_adm_shp/BEN_adm0.shp')
COU.read_shapefile('some_location_where_large_raw_data_is_stored/BEN_adm_shp/BEN_adm0.shp', long_name='NAME_LOCAL', short_name='ISO')
COU.read_shapefile('some_location_where_large_raw_data_is_stored/BEN_adm_shp/BEN_adm1.shp')
COU.read_shapefile('some_location_where_large_raw_data_is_stored/BEN_adm_shp/BEN_adm1.shp', long_name='NAME_1', short_name='HASC_1')
COU.read_shapefile('some_location_where_large_raw_data_is_stored/BEN_adm_shp/BEN_adm2.shp')
COU.read_shapefile('some_location_where_large_raw_data_is_stored/BEN_adm_shp/BEN_adm2.shp', long_name='NAME_2', short_name='HASC_2')

COU.load_shapefile()
COU.create_masks_overlap(input_file='some_location_where_large_raw_data_is_stored/mon_JRA55_002_prmsl.nc')
COU.create_masks_latweighted()
COU.load_mask()
COU.zoom_data(input_file='some_location_where_large_raw_data_is_stored/mon_JRA55_002_prmsl.nc',var_name='var2',given_var_name='psl',tags={'source':'JRA55'})
COU.zoom_data(input_file='some_location_where_large_raw_data_is_stored/mon_JRA55_033_ugrd.nc',
				var_name='var33',given_var_name='uWind',tags={'source':'JRA55'},
				input_array=xr.open_dataset('some_location_where_large_raw_data_is_stored/mon_JRA55_033_ugrd.nc')['var33'].loc[:,85000,:,:])

COU.meta_gather_information()
COU.meta_load()
COU.griddedData_read()
COU.griddedData_print()

COU.calculate_areaAverage()

COU = class_on_shape.shaped_object(iso='DEU', working_directory='small_data/DEU')
COU.read_shapefile('some_location_where_large_raw_data_is_stored/gadm36_DEU_shp/gadm36_DEU_0.shp')
COU.read_shapefile('some_location_where_large_raw_data_is_stored/gadm36_DEU_shp/gadm36_DEU_0.shp', long_name='NAME_0', short_name='GID_0')
COU.read_shapefile('some_location_where_large_raw_data_is_stored/gadm36_DEU_shp/gadm36_DEU_1.shp')
COU.read_shapefile('some_location_where_large_raw_data_is_stored/gadm36_DEU_shp/gadm36_DEU_1.shp', long_name='NAME_1', short_name='GID_1')
# COU.read_shapefile('some_location_where_large_raw_data_is_stored/gadm36_DEU_shp/gadm36_DEU_2.shp')
# COU.read_shapefile('some_location_where_large_raw_data_is_stored/gadm36_DEU_shp/gadm36_DEU_2.shp', long_name='NAME_2', short_name='gid_2')

COU.load_shapefile()
COU.create_masks_overlap(input_file='/Users/peterpfleiderer/Projects/data/JRA55/mon_JRA55_002_prmsl.nc')
COU.create_masks_latweighted()
COU.load_mask()
COU.zoom_data(input_file='some_location_where_large_raw_data_is_stored/mon_JRA55_002_prmsl.nc',var_name='var2',given_var_name='psl',tags={'source':'JRA55'})
COU.zoom_data(input_file='some_location_where_large_raw_data_is_stored/mon_JRA55_033_ugrd.nc',
				var_name='var33',given_var_name='uWind',tags={'source':'JRA55'},
				input_array=xr.open_dataset('some_location_where_large_raw_data_is_stored/mon_JRA55_033_ugrd.nc')['var33'].loc[:,85000,:,:])


COU.meta_gather_information()
COU.meta_load()
COU.griddedData_read()
COU.griddedData_print()

COU.calculate_areaAverage()




#
